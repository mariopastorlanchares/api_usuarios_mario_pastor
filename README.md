# README #

Api en Symfony 5. Prueba Mario Pastor Lanchares

### Resumen ###

Administración de Usuarios y Grupos de Usuarios.<br/>
Registro de estadísticas simple de lo que está pasando.

Métodos a realizar:
* Crear usuario
* Crear grupo
* Asignar usuario a grupo

### Instalación ###
* [Instalar Symfony](https://symfony.com/download):

```
     wget https://get.symfony.com/cli/installer -O - | bash
```
* Instalar dependencias (nos situamos en la raíz del proyecto):

```
     cd /api_usuarios_mario_pastor
     composer install
```
* Base de datos:<br>
Se ha utilizado una base de datos en MySql:
  * Crear usuario db:
  ```mysql
  CREATE USER 'user_api_mp'@'localhost' IDENTIFIED BY 'password_for_user_api_mp';
  GRANT ALL PRIVILEGES ON *.* TO 'user_api_mp'@'localhost';
  FLUSH PRIVILEGES;
  ```
  * Crear base de datos:

  ```
    php bin/console doctrine:database:create
  ```
  * Aplicar migraciones:

  ```
    php bin/console doctrine:migrations:migrate
  ```
  
### Caso de uso ###

* Arrancar la API: 
  ```
    symfony:server:start
  ```
* Crear un usuario administrador:<br>
Para poder realizar la mayoría de llamadas a la api, tendremos que hacerlo como usuarios administradores.<br>
  Se ha creado un comando especial para la creación de un usuario administrador, que nos devolverá nuestra apiKey.<br>
  Nos preguntará un nombre e email de usuario y nos proveerá la clave<>
  (identificador a la hora de hacer llamadas a la API)
  ```
    bin/console app:user-create
  ```

  
* Consultar la información del usuario recien creado:<br>
  Para poder hacer llamadas a la API es recomendable utilizar POSTMAN o algún software similar.<br>
  Una vez en Postman es necesario incluir la siguiente clave/valor en los Headers:<br>
    ```
  KEY: X-AUTH-TOKEN | VALUE: nuestra_api_key_administrador
  ```

  <b>IMPORTANTE: Siempre que hagamos llamadas a cualquier ruta dentro de /api necesitamos estar identificados
  con un apiKey, ya sea administrador o usuario estándar.</b><br>
  Una vez incluido el apiKey ya estaremos identificados y podemos consultar nuestra información de 
  usuario haciendo una llamada GET a la siguiente URL.

    ```
    GET | http://127.0.0.1:8000/api/users/my_info
    ```
* Crear usuario:<br>
  La creación de usuarios es un método restringido a los usuarios con rol de administrador, y generará un usuario estándar, 
  sin permisos de administrador.<br>
  A modo de ejemplo, podemos crear un usuario introduciendo el siguiente Body a nuestra petición:
    ```
    {
    "name": "mario.pastor",
    "email": "mario.pastor@moovity.io"
    }
    ```
  Ya podemos realizar la llamada POST a la API:
    ```
    POST | http://127.0.0.1:8000/api/users/add
    ```
* Crear grupo de usuarios:<br>
  La creación de grupos de usuarios es un método restringido a los usuarios con rol de administrador.<br>
  Vamos a crear un grupo de usuarios llamado "administradores":
    ```
    {
    "name": "administradores",
    "description": "Los administradores del sistema"
    }
    ```
  Ya podemos realizar la llamada POST a la API:
    ```
    POST | http://127.0.0.1:8000/api/user_groups/add
    ```
  Nos devolverá el id del grupo, que podemos utilizar para asignar usuarios al mismo

* Asignar usuarios a grupos:<br>
  Hay varios métodos para agregar los usuarios a los grupos
  * Método 1: setear los grupos con el método setUserGroup.<br>
    Request body, guarda un array de los ids de los grupos que queremos asignar:
    ```
    {
    "user_group_ids": [1, 2, 3]
    }
    ```
    URL, enviamos por url el id del usuario al que queremos asignar a los grupos:

    ```
      POST | http://127.0.0.1:8000/api/users/setgroups/1
    ```

  * Método 2: editar el usuario con editUser.<br>
    Request body, guarda los datos que quereoms editar, incluyendo los grupos:
    ```
    {
      "name": "mario.pastor",
      "email": "mario.pastor@moovity.io",
      "userGroups": {
        "0": {
            "id": "1"
        },
        "1": {
            "id": "2"
        },
        "2": {
            "id": "3"
        }
      }
    }
    ```
    URL, enviamos por url el id del usuario al que queremos asignar a los grupos:

    ```
      POST | http://127.0.0.1:8000/api/users/edit/1
    ```

* Desvincular usuarios de grupos:<br>
  Request body, guarda un array de los ids de los grupos que queremos desvincular:
  ```
  {
  "user_group_ids": [1, 2, 3]
  }
    ```
    URL, enviamos por url el id del usuario:

    ```
      POST | http://127.0.0.1:8000/api/users/removegroups/2
    ```
* Guardados estadísticos:<br>
  Para llevar constancia de lo que va sucediendo en la aplicación, se ha creado una entidad <b>Statistic</b> 
  y un Listener que escuchará cada vez que se realice un evento de doctrine, guardando todos los datos
  susceptibles de ser monitorizados:

    ```
      GET | http://127.0.0.1:8000/api/statistics/list
    ```

### Detalles ###
* Además de los registros estadísticos en bd, podemos encontrar un completo 
  log creado con Monolog en var/log dependiendo del entorno dev.log, prod.log, etc.

### Autoría ###

* Realizado por Mario Pastor
