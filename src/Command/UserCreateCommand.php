<?php


namespace App\Command;


use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserFormProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class UserCreateCommand extends Command
{
    private $formProcessor;
    private $userRepository;

    public function __construct(UserFormProcessor $formProcessor, UserRepository $userRepository, string $name = null)
    {
        $this->userRepository = $userRepository;
        $this->formProcessor = $formProcessor;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('app:user-create')
            ->setDescription('Crea un nuevo usuario administrador');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');
        $output->writeln('<info>[INFO]</info> Se va a crear un nuevo usuario con permisos ROLE_ADMIN.');
        $output->writeln('');
        $user = new User();
        $name = $this->askUserName($input, $output);
        $output->writeln('');
        $email = $this->askUserEmail($input, $output);
        $output->writeln('');
        $user->setName($name);
        $user->setEmail($email);
        $user->setRoles(['ROLE_ADMIN']);
        $this->formProcessor->saveUser($user);
        $output->writeln('API Key administrador: ' . $user->getApiKey());
        $output->writeln('');
        return Command::SUCCESS;
    }

    private function askUserName(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question('Especifica el nombre de usuario: ');
        $name = $helper->ask($input, $output, $question);
        $errors = [];
        if (strlen($name) < 5) {
            $errors[] = '<error>[ERROR]</error> El nombre de usuario debe tener 5 o más caracteres.';
        }
        if ($error = $this->userRepository->findBy(['name' => $name])) {
            $errors[] = '<error>[ERROR]</error> El nombre de usuario ya existe.';
        }
        if (!empty($errors)) {
            $this->displayErrors($errors, $output);
            return $this->askUserName($input, $output);
        } else {
            return $name;
        }
    }

    private function askUserEmail(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question('Especifica el email de usuario: ');
        $email = $helper->ask($input, $output, $question);
        $errors = [];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = '<error>[ERROR]</error> El email no es válido. ';
        }
        if ($error = $this->userRepository->findBy(['email' => $email])) {
            $errors[] = '<error>[ERROR]</error> El email ya existe. ';
        }
        if (!empty($errors)) {
            $this->displayErrors($errors, $output);
            return $this->askUserEmail($input, $output);
        } else {
            return $email;
        }
    }

    private function displayErrors($errors, OutputInterface $output)
    {
        foreach ($errors as $error) {
            $output->writeln($error);
        }
        $output->writeln('');
    }

}