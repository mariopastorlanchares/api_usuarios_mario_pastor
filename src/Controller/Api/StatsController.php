<?php


namespace App\Controller\Api;


use App\Entity\Statistic;
use App\Repository\StatsRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class StatsController extends AbstractFOSRestController
{
    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Get(path="/statistics/list", name="statistics_list")
     * @Rest\View(serializerGroups={"statistic"})
     * @param StatsRepository $statsRepository
     * @return Statistic[]
     */
    public function listUsers(StatsRepository $statsRepository)
    {
        return $statsRepository->findAll();
    }

}