<?php


namespace App\Controller\Api;


use App\Entity\User;
use App\Form\Type\UserFormType;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;
use App\Service\UserFormProcessor;
use App\Service\UserService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserController extends AbstractFOSRestController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Get(path="/users/list", name="users_list")
     * @Rest\View(serializerGroups={"basic"})
     * @param UserRepository $userRepository
     * @return User[]
     */
    public function listUsers(UserRepository $userRepository)
    {
        $this->logger->info('Usuarios listados');
        return $userRepository->findAll();
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="users/get/{id}", name="users_get")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param int $id
     * @param UserRepository $userRepository
     * @return User|null
     */
    public function getUserById(int $id, UserRepository $userRepository)
    {
        return $userRepository->find($id);
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post("/users/add", name="users_add")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param Request $request
     * @param UserFormProcessor $userFormProcessor
     * @return User|FormInterface
     */
    public function addUser(Request $request, UserFormProcessor $userFormProcessor)
    {
        $user = new User();
        [$user, $error] = ($userFormProcessor)($user, $request);
        return $user ?? $error;
    }


    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="users/edit/{id}", name="users_edit")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param int $id
     * @param UserFormProcessor $userFormProcessor
     * @param UserRepository $userRepository
     * @param Request $request
     * @return View|mixed
     */
    public function editUser(Request $request, int $id, UserFormProcessor $userFormProcessor, UserRepository $userRepository)
    {
        $user = $userRepository->find($id);
        if (!$user) {
            return View::create('Usuario no encontrado', Response::HTTP_BAD_REQUEST);
        }
        [$user, $error] = ($userFormProcessor)($user, $request);
        return $user ?? $error;
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="users/setgroups/{id}", name="users_set_groups")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param Request $request
     * @param int $id
     */
    public function setUserGroup(Request $request, int $id, UserService $userService, UserRepository $userRepository)
    {
        $userGroupIds = $request->get('user_group_ids');
        $user = $userRepository->find($id);
        if (empty($userGroupIds) || !$user) {
            return View::create('Parámetros incorrectos', Response::HTTP_BAD_REQUEST);
        }
        return $userService->setUserGroups($user, $userGroupIds);
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="users/removegroups/{id}", name="users_remove_groups")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param Request $request
     * @param int $id
     */
    public function removeUserGroup(Request $request, int $id, UserService $userService, UserRepository $userRepository)
    {
        $userGroupIds = $request->get('user_group_ids');
        $user = $userRepository->find($id);
        if (empty($userGroupIds) || !$user) {
            return View::create('Parámetros incorrectos', Response::HTTP_BAD_REQUEST);
        }
        return $userService->removeUserGroups($user, $userGroupIds);
    }

    /**
     * @Rest\Get(path="users/my_info", name="users_my_info")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param UserRepository $userRepository
     * @return View|mixed
     */
    public function getMyUserInfo(UserRepository $userRepository)
    {
        return $this->getUser();
    }

}