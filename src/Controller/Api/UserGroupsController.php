<?php


namespace App\Controller\Api;



use App\Entity\UserGroup;
use App\Repository\UserGroupRepository;
use App\Service\UserGroupFormProcessor;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserGroupsController extends AbstractFOSRestController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Get(path="/user_groups/list", name="user_groups_list")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param UserGroupRepository $userGroupRepository
     * @return UserGroup[]
     */
    public function listGroups(UserGroupRepository $userGroupRepository)
    {
        $this->logger->info('Grupos de usuarios listados');
        return $userGroupRepository->findAll();
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="user_groups/get/{id}", name="user_groups_get")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param int $id
     * @param UserGroupRepository $userGroupRepository
     * @return UserGroup|null
     */
    public function getGroupById(int $id, UserGroupRepository $userGroupRepository)
    {
        return $userGroupRepository->find($id);
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post("/user_groups/add", name="user_groups_add")
     * @Rest\View(serializerGroups={"basic"})
     * @param Request $request
     * @param UserGroupFormProcessor $userGroupFormProcessor
     * @return mixed
     */
    public function addGroup(Request $request, UserGroupFormProcessor $userGroupFormProcessor)
    {
        $userGroup = new UserGroup();
        [$userGroup, $error] = ($userGroupFormProcessor)($userGroup, $request);
        return $userGroup ?? $error;
    }

    /**
     * @isGranted("ROLE_ADMIN",  message="Necesitas permisos de administrador")
     * @Rest\Post(path="user_groups/edit/{id}", name="users_groups_edit")
     * @Rest\View (serializerGroups={"user", "basic"})
     * @param int $id
     * @param UserGroupFormProcessor $userGroupFormProcessor
     * @param UserGroupRepository $userGroupRepository
     * @param Request $request
     * @return View|mixed
     */
    public function editGroup(int $id, UserGroupFormProcessor $userGroupFormProcessor, UserGroupRepository $userGroupRepository, Request $request)
    {
        $group = $userGroupRepository->find($id);
        if (!$group) {
            return View::create('Grupo no encontrado', Response::HTTP_BAD_REQUEST);
        }
        [$group, $error] = ($userGroupFormProcessor)($group, $request);
        return $group ?? $error;
    }

}