<?php


namespace App\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class IndexController extends AbstractController
{
    /**

     * @Rest\Get("/")
     * @return Response
     */
    public function index(){
        $status = [
            "aplication_name"=>"API Server Usuarios",
            "server_time"=>date("Y-m-d H:i:s"),
            "env"=>$_ENV['APP_ENV'],
        ];
        return new JsonResponse($status);
    }
}