<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;


/**
 * @ORM\Entity
 * *
 * @package App\Entity
 */
class Statistic
{

    /**
     * @Groups({"statistic"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"statistic"})
     * Entidad a la que hace referencia el cambio
     * @ORM\Column(type="string")
     * @Assert\NotNull
     *
     * @var string
     */
    private $entity;

    /**
     * @Groups({"statistic"})
     * Id de la entidad a la que hace referencia el cambio
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     *
     * @var int
     */
    private $entity_id;

    /**
     * @Groups({"statistic"})
     * Tipo de operación (Creación, modificación..)
     * @ORM\Column(type="string")
     * @Assert\NotNull
     *
     * @var string
     */
    private $type;

    /**
     * @Groups({"statistic"})
     * Relación con el usuario que ha realizado la acción
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="auditorias")
     */
    private $user;

    /**
     * @Groups({"statistic"})
     * Fecha en la que se ha realizado la operación
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     *
     */
    private $date;

    /**
     * @Groups({"statistic"})
     * IP desde la que se ha realizado la operación
     *
     * @ORM\Column(type="string")
     *
     */
    private $ip;

    /**
     * @Groups({"statistic"})
     * Array con el listado de campos actualizados en la operación en caso de ser una actualización
     * @ORM\Column(type="array", nullable=true)
     *
     * @var array
     */
    private $updatedFields;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity(string $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entity_id;
    }

    /**
     * @param int $entity_id
     */
    public function setEntityId(int $entity_id): void
    {
        $this->entity_id = $entity_id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Statistic
     */
    public function setUser(?User $user): self
    {
        if($user) {
            $this->user = $user;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return Statistic
     */
    public function setDate(\DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return Statistic
     */
    public function setIp(string $ip): self
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return array
     */
    public function getUpdatedFields(): array
    {
        return $this->updatedFields;
    }

    /**
     * @param array $updatedFields
     */
    public function setUpdatedFields(array $updatedFields): void
    {
        $this->updatedFields = $updatedFields;
    }

}