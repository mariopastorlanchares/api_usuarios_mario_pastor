<?php

namespace App\EventListener;


use App\Entity\Statistic;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class StatsListener
{
    private $tokenStorage;
    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(RequestStack $requestStack, TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $em = $args->getObjectManager();
        $entity = $args->getObject();
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSets(); // do not compute changes if inside a listener
        $changeset = $uow->getEntityChangeSet($entity);
        if (method_exists($entity, "toArray")) {
            $entityArray = $entity->toArray();
            foreach ($entityArray as $k => $v) {
                $method = sprintf('get%s', ucwords($k));
                if (method_exists($entity, $method)) {

                    $var = $entity->$method();
                    if ($var instanceof PersistentCollection and (count($var->getInsertDiff()) > 0 or count($var->getDeleteDiff()) > 0)) {
                        $array = [$var->getSnapshot(), $var];
                        $i = 0;
                        foreach ($array as $item) {
                            $result = "";
                            foreach ($item as $val) {
                                $result .= ((method_exists($val, "toString")) ? $val->toString() : $val->getId()) . ", ";
                            }
                            $changeset[$k][$i] = rtrim($result, ", ");
                            $i++;
                        }
                    }
                }
            }
        }
        $status = "update";
        $noSaveValueField = [
            'apiKey'
        ];
        foreach ($changeset as $k => $v) {
            if (in_array($k, $noSaveValueField)) {
                $changeset[$k] = ["**********", "**********"];
            }
        }
        if (count($changeset)) {
            $this->createStat($em, $entity, $status, $changeset);
        }
        return;
    }

    public function postPersist(LifecycleEventArgs $args)
    {

        $em = $args->getObjectManager();
        $entity = $args->getObject();
        $this->createStat($em, $entity, 'create');
        return;
    }

    public function createStat($em, $entity, $type, $changeset = [])
    {
        if (
            $entity instanceof Statistic
        ) {
            return;
        }
        $audit = new Statistic();
        $audit->setEntity(get_class($entity));
        $audit->setEntityId($entity->getId());
        if (null !== $currentUser = $this->getUser()) {
            $audit->setUser($currentUser);
        } else {
            $audit->setUser(null);
        }
        $audit->setType($type);
        $audit->setDate(new \DateTime());
        $audit->setIp($this->resolveClientIp());
        $audit->setUpdatedFields($changeset);
        $em->persist($audit);
        $em->flush();
    }

    public function getUser()
    {
        if (!$this->tokenStorage) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }

    /**
     * @return string
     */
    protected function resolveClientIp()
    {
        $masterRequest = $this->requestStack->getMasterRequest();
        if ($masterRequest != null) {
            return $masterRequest->getClientIp();
        } else {
            return "";
        }
    }
}