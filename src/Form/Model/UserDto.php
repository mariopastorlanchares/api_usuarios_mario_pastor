<?php


namespace App\Form\Model;


use App\Entity\User;

class UserDto
{
    public $name;
    public $email;
    public $userGroups;

    public function __construct()
    {
        $this->userGroups = [];
    }

    public static function createFromUser(User $user): self
    {
        $dto = new self();
        $dto->name = $user->getName();
        $dto->email = $user->getEmail();
        return $dto;
    }
}