<?php


namespace App\Form\Model;



use App\Entity\UserGroup;

class UserGroupDto
{
    public $id;
    public $name;
    public $description;

    public static function createFromUserGroup(UserGroup $userGroup): self
    {
        $dto = new self();
        $dto->id = $userGroup->getId();
        $dto->name = $userGroup->getName();
        $dto->description = $userGroup->getDescription();
        return $dto;
    }
}