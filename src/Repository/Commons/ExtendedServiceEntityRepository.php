<?php

declare(strict_types=1);


namespace App\Repository\Commons;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ConnectionException;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Implementación de la interfaz general para repositorios con métodos adicionales.
 */
abstract class ExtendedServiceEntityRepository extends ServiceEntityRepository
{


    public function beginTransaction()
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
    }


    public function commit()
    {
        $this->getEntityManager()->getConnection()->commit();
    }


    public function rollBack()
    {
        $this->getEntityManager()->getConnection()->rollBack();
    }


    public function merge($entity)
    {
        return $this->getEntityManager()->merge($entity);
    }


    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }


    public function flush($entity = null)
    {
        $this->getEntityManager()->flush($entity);
    }


    public function getFilter($key, $value, $comparacion = 'like')
    {

        $filterApply = array(
            'comparation' => $comparacion,
            'key' => $key,
            'value' => $value,

        );
        return $filterApply;
    }


    public function createSimpleQueryBuilder($alias, array $fields)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select($fields)
            ->from($this->_entityName, $alias);

        return $qb;
    }

    public function save($entity)
    {
            $this->persist($entity);
            $this->flush($entity);
            return $entity;
    }

    public function update($entity)
    {
        $this->merge($entity);
        $this->flush($entity);
        return $entity;
    }
}
