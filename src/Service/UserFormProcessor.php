<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\UserGroup;
use App\Form\Model\UserDto;
use App\Form\Model\UserGroupDto;
use App\Form\Type\UserFormType;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;
use App\Security\TokenAuthenticator;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFormProcessor
{
    private $repository;
    private $tokenAuthenticator;
    private $logger;
    private $formFactory;
    private $userGroupRepository;

    public function __construct(UserRepository $userRepository,
                                UserGroupRepository $userGroupRepository,
                                TokenAuthenticator $tokenAuthenticator,
                                LoggerInterface $logger,
                                FormFactoryInterface $formFactory)
    {
        $this->repository = $userRepository;
        $this->userGroupRepository = $userGroupRepository;
        $this->tokenAuthenticator = $tokenAuthenticator;
        $this->logger = $logger;
        $this->formFactory = $formFactory;
    }

    public function __invoke(User $user, Request $request): array
    {
        $type = ($user->getId()) ? 'Edición' : 'Creación';
        $form = $this->formFactory->create(UserFormType::class, $user);
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            return [null, 'Form not submitted'];
        }
        if ($form->isValid()) {
            $prevGroups = $user->getUserGroups();
            $user->setUserGroups(new ArrayCollection());
            $this->saveUser($user);
            foreach ($prevGroups as $group) {
                if ($group->getId() != null) {
                    $group = $this->userGroupRepository->find($group->getId());
                }
                if ($group != null){
                    $group->addUser($user);
                    $user->addUserGroup($group);
                    $this->userGroupRepository->save($group);
                }
            }
            $this->logger->info("$type Usuario: " . $user->getName());
            return [$user, null];
        }
        $this->logger->error("Formulario $type de Usuario:  NO VÁLIDO");
        return [null, $form];
    }

    public function saveUser(&$user)
    {
        if (empty($user->getId())) {
            //Nuevo usuario
            $user->setCreatedAt(new \DateTime());
            $user->setApiKey($this->tokenAuthenticator->generateApiKey());
            $this->repository->save($user);
        } else {
            $this->repository->update($user);
        }
        return $user;
    }
}