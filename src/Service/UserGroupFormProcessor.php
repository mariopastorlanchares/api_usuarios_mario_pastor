<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\UserGroup;
use App\Form\Type\UserFormType;
use App\Form\Type\SimpleUserGroupFormType;
use App\Form\Type\UserGroupFormType;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;
use App\Security\TokenAuthenticator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserGroupFormProcessor
{
    private $repository;
    private $tokenAuthenticator;
    private $logger;
    private $formFactory;

    public function __construct(UserGroupRepository $userRepository,
                                TokenAuthenticator $tokenAuthenticator,
                                LoggerInterface $logger,
                                FormFactoryInterface $formFactory)
    {
        $this->repository = $userRepository;
        $this->tokenAuthenticator = $tokenAuthenticator;
        $this->logger = $logger;
        $this->formFactory = $formFactory;
    }

    public function __invoke(UserGroup $userGroup, Request $request): array
    {

        $form = $this->formFactory->create(UserGroupFormType::class, $userGroup);

        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return [null, 'Form not submitted'];
        }
        if ($form->isValid()) {
            $this->saveUserGroup($userGroup);
            $this->logger->info('Grupo de usuarios creado/editado: ' . $userGroup->getName());
            return [$userGroup, null];
        }
        $this->logger->error('Formulario creación/edición grupo de usuarios:  NO VÁLIDO');
        return [null, $form];
    }

    public function saveUserGroup(&$userGroup)
    {
        if (empty($userGroup->getId())) {
            //Nuevo grupo de usuarios
            $userGroup->setCreatedAt(new \DateTime());
        }
        $this->repository->save($userGroup);
        return $userGroup;
    }
}