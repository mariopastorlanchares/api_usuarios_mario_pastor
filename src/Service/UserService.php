<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;

class UserService
{
    private $repository;
    private $userGroupRepository;

    public function __construct(UserRepository $userRepository, UserGroupRepository $userGroupRepository)
    {
        $this->repository = $userRepository;
        $this->userGroupRepository = $userGroupRepository;
    }

    public function setUserGroups(User $user, array $userGroupIds)
    {
        $userGroups = $this->userGroupRepository->findBy(['id' => $userGroupIds]);
        foreach ($userGroups as $group) {
            $group->addUser($user);
            $this->userGroupRepository->save($group);
        }
        return $this->repository->save($user);
    }

    public function removeUserGroups(User $user, array $userGroupIds)
    {
        $userGroups = $this->userGroupRepository->findBy(['id' => $userGroupIds]);
        foreach ($userGroups as $group) {
            $group->removeUser($user);
            $this->userGroupRepository->save($group);
        }
        return $this->repository->save($user);
    }
}